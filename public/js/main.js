import { tap, delay } from 'rxjs/operators';
import { fromEvent } from 'rxjs/index';

const btn = document.getElementById('sbj1')
const badge1 = document.getElementById('ob1')
const badge2 = document.getElementById('ob2')
const loader = document.getElementById('radio')

// ボタンのクリックイベントからsubjectを作成
const subject = fromEvent(btn, 'click')

// Observerの生成
const observer1 = {
  next: () => {
  badge1.classList.toggle('bg-secondary')
  badge1.classList.toggle('bg-danger')
  }
}

// Observerの生成
const observer2 = {
  next: () => {
    badge2.classList.toggle('bg-secondary')
    badge2.classList.toggle('bg-warning')
    }
}

// Observerの登録
subject.subscribe(observer1);
subject.subscribe(observer2);

// アニメーション
subject.pipe(
  tap(() => loader.classList.toggle('loader')),
  delay(800)
)
.subscribe(() => {
  loader.classList.toggle('loader')
})