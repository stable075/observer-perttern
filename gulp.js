var fs = require('fs');
var hljs = require('highlight.js');

function highlight(file, type){
    var data = fs.readFileSync(file); //ファイルの内容を取得
    //highlight(言語パターン, 内容)でクラスを付与したデータを返します
    fs.writeFileSync('code.html', hljs.highlight(type, data.toString()).value);
}

highlight('./public/js/main.js', 'js')
